cmake_minimum_required(VERSION 3.15)
project(openmp_mpi)

set(CMAKE_VERBOSE_MAKEFILE ON)
set(CMAKE_CXX_STANDARD 11)

include(CheckCXXCompilerFlag)

CHECK_CXX_COMPILER_FLAG("-mtune=native" MTUNE_NATIVE)
if (MTUNE_NATIVE)
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -mtune=native")
endif()

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Ofast -fno-tree-vectorize -Wall")

add_executable(${PROJECT_NAME} "")
add_subdirectory(src)

find_package(OpenMP REQUIRED)
find_package(MPI REQUIRED)

set(CMAKE_CXX_COMPILE_FLAGS ${CMAKE_CXX_COMPILE_FLAGS} ${MPI_CXX_COMPILE_FLAGS})
set(CMAKE_CXX_LINK_FLAGS ${CMAKE_CXX_LINK_FLAGS} ${MPI_CXX_LINK_FLAGS})

target_link_libraries(${PROJECT_NAME} PRIVATE OpenMP::OpenMP_CXX MPI::MPI_CXX)
