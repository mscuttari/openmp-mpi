#include <cstring>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <queue>
#include <mpi.h>
#include "reader.h"
#include "timer.h"
#include "worker.h"

#define CHUNK_SIZE_DEFAULT  300

#define MESSAGE_COLUMNS     1
#define MESSAGE_OFFSETS     2
#define MESSAGE_FINISHED    3

/**
 * Similar to the offsets streaming solution, but the offsets are kept
 * in a queue and sent whenever a worker becomes available. The master
 * process just manages the coordination and not contribute to the
 * processing, but it can be extended to do it.
 * This solution allows for heterogeneous workers to not slow down the
 * faster ones.
 */

using namespace std;

struct Timers {
    Timers() = default;
    Timer total;
    Timer loading;
    Timer communication;
    Timer processing;
    Timer waiting;
    Timer output;
};

extern int offsets_queue(int argc, char *argv[]) {
    int rank, nProcs, chunkSize;
    Timers timers;

    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &nProcs);

    if (nProcs == 1) {
        cerr << "At least two processes required" << endl;
        return EXIT_FAILURE;
    }

    if (rank == 0 && argc < 3) {
        cerr << "Usage: " << argv[0] << " <data_file> <output_file> [chunk_size]" << endl;
        return EXIT_FAILURE;
    }

    if (argc == 4) {
        chunkSize = strtol(argv[3], nullptr, 10);
    } else {
        chunkSize = CHUNK_SIZE_DEFAULT;
    }

    timers.total.start();

    // Open the input file on all the processes
    ifstream input(argv[1], ios::binary);

    if (!input.is_open()) {
        cerr << "Could not open the input file " << argv[1] << endl;
        return EXIT_FAILURE;
    }

    int size = getFileSize(input);

    if (rank == 0) {
        cout << "Chunk size: " << chunkSize << endl;
        cout << "File size: " << size << " bytes" << endl;
    }

    MPI_Barrier(MPI_COMM_WORLD);

    if (rank == 0) {
        // Master process
        ofstream output(argv[2]);

        if (!output.is_open()) {
            cerr << "Could not open the output file " << argv[2] << endl;
            return EXIT_FAILURE;
        }

        Results results;

        queue<int> processesQueue;
        queue<int> chunksQueue;
        bool *chunksAvailability = new bool[nProcs - 1];
        int *processedChunksCounters = new int[nProcs - 1];

        for (int i = 0; i < nProcs - 1; i++) {
            processesQueue.push(i + 1);     // All the processes (except the master) are initially available
            chunksAvailability[i] = false;  // No chunks are initially available to be sent
            processedChunksCounters[i] = 0;
        }

        // Skip the initial BOM bytes
        input.seekg(3);

        // Get the columns line
        string columnsLine;
        getline(input, columnsLine);

        if (columnsLine[columnsLine.length() - 1] == '\r') {
            columnsLine.resize(columnsLine.length() - 1);
        }

        // Send the columns line to all the other processes
        for (int destination = 1; destination < nProcs; destination++) {
            MPI_Request request;

            timers.communication.start();
            MPI_Isend(columnsLine.c_str(), columnsLine.length(), MPI_CHAR, destination, MESSAGE_COLUMNS, MPI_COMM_WORLD, &request);
            MPI_Wait(&request, MPI_STATUS_IGNORE);
            timers.communication.stop();
        }

        // Chunks to be distributed among the processes
        int *chunks = new int[(nProcs - 1) * 2];

        int position = input.tellg();

        cout << std::fixed << std::setprecision(2);
        cout << "Progress: 0.00%";

        do {
            // Create new chunks
            for (int i = 0; i < nProcs - 1; i++) {
                if (position == size || position == -1) {
                    // Finished reading the input
                    break;
                }

                // If the i-th slot is not available, it means it has not been populated with valid
                // offsets. So we can proceed to populate it.
                if (!chunksAvailability[i]) {
                    timers.loading.start();

                    memset(&(chunks[i * 2]), 0, sizeof(int) * 2);
                    chunks[i * 2] = position;

                    int lines = 0;
                    for (string line; lines < chunkSize && getline(input, line); lines++);

                    position = input.tellg();

                    if (position == -1) {
                        // End of file reached
                        chunks[i * 2 + 1] = size;
                    } else {
                        chunks[i * 2 + 1] = position;
                    }

                    chunksQueue.push(i);
                    chunksAvailability[i] = true;

                    timers.loading.stop();
                }
            }

            // Send the chunks
            while (!processesQueue.empty() && !chunksQueue.empty()) {
                int destination = processesQueue.front();
                processesQueue.pop();

                int chunkNumber = chunksQueue.front();
                chunksQueue.pop();
                chunksAvailability[chunkNumber] = false;

                MPI_Request request;

                timers.communication.start();
                MPI_Isend(&(chunks[chunkNumber * 2]), 2, MPI_INT, destination, MESSAGE_OFFSETS, MPI_COMM_WORLD, &request);
                MPI_Wait(&request, MPI_STATUS_IGNORE);
                timers.communication.stop();

                processedChunksCounters[destination - 1]++;
            }

            // If the chunk queue is full, wait for at least one process to finish his job
            if ((int) chunksQueue.size() == nProcs - 1) {
                timers.waiting.start();
                MPI_Probe(MPI_ANY_SOURCE, MESSAGE_FINISHED, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
                timers.waiting.stop();
            }

            // Check if some process has finished computing its chunk
            timers.communication.start();
            int flag = 0;

            do {
                MPI_Status status;
                MPI_Iprobe(MPI_ANY_SOURCE, MESSAGE_FINISHED, MPI_COMM_WORLD, &flag, &status);

                if (flag) {
                    MPI_Recv(nullptr, 0, MPI_INT, status.MPI_SOURCE, status.MPI_TAG, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
                    processesQueue.push(status.MPI_SOURCE);
                }
            } while (flag);

            timers.communication.stop();

            // Show progress
            long double progress = position == -1 ? 100 : (long double) position / size * 100;
            cout << "\rProgress: " << progress << "%";

        } while ((position < size && position != -1) || !chunksQueue.empty());

        // Wait for all the processes to finish their chunk
        while ((int) processesQueue.size() != nProcs - 1) {
            MPI_Status status;

            timers.waiting.start();
            MPI_Recv(nullptr, 0, MPI_INT, MPI_ANY_SOURCE, MESSAGE_FINISHED, MPI_COMM_WORLD, &status);
            timers.waiting.stop();

            processesQueue.push(status.MPI_SOURCE);
        }

        cout << "\rProgress: 100.00%\n" << endl;

        // Stop all the workers by sending them a zero size chunk
        while (!processesQueue.empty()) {
            int stopOffsets[2] = {0, 0};
            int destination = processesQueue.front();
            processesQueue.pop();

            timers.communication.start();
            MPI_Request request;
            MPI_Isend(stopOffsets, 2, MPI_INT, destination, MESSAGE_OFFSETS, MPI_COMM_WORLD, &request);
            MPI_Wait(&request, MPI_STATUS_IGNORE);
            timers.communication.stop();
        }

        // Collect results
        timers.communication.start();
        results.collect(0, MPI_COMM_WORLD);
        timers.communication.stop();

        // Write results to file
        if (rank == 0) {
            timers.output.start();
            results.write(output);
            timers.output.stop();
        }

        timers.total.stop();

        // Get the time statistics of the workers
        unsigned long long localProcessingTime = timers.processing.elapsed();
        unsigned long long averageProcessingTime;

        MPI_Reduce(&localProcessingTime, &averageProcessingTime, 1, MPI_UNSIGNED_LONG_LONG, MPI_SUM, 0, MPI_COMM_WORLD);
        averageProcessingTime /= (nProcs - 1);

        cout << "Processed chunks:\n";

        for (int i = 0; i < nProcs - 1; i++) {
            cout << " - Process " << i + 1 << ": " << processedChunksCounters[i] << "\n";
        }

        cout << endl;

        cout << "Time statistics:\n"
             << " - Total: "            << timers.total.elapsed()         << " ms\n"
             << " - Loading: "          << timers.loading.elapsed()       << " ms\n"
             << " - Communication: "    << timers.communication.elapsed() << " ms\n"
             << " - Processing (avg): " << averageProcessingTime          << " ms\n"
             << " - Waiting: "          << timers.waiting.elapsed()       << " ms\n"
             << " - Output: "           << timers.output.elapsed()        << " ms\n"
             << endl;

        // Free the resources
        delete[] chunksAvailability;
        delete[] processedChunksCounters;
        delete[] chunks;

    } else {
        // Worker process
        timers.total.start();

        // Receive the columns line
        string columnsLine;

        {
            MPI_Status status;

            // Wait for the columns message
            timers.waiting.start();
            MPI_Probe(0, MESSAGE_COLUMNS, MPI_COMM_WORLD, &status);
            timers.waiting.stop();

            // Allocate enough space for the line
            int length;
            MPI_Get_count(&status, MPI_CHAR, &length);
            columnsLine.resize(length);

            // Receive it
            timers.communication.start();
            MPI_Recv((char*) columnsLine.c_str(), length, MPI_CHAR, 0, MESSAGE_COLUMNS, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
            timers.communication.stop();
        }

        timers.processing.start();
        map<string, int> columns = mapColumns(columnsLine);
        timers.processing.stop();

        int offsets[2];
        Results results;

        timers.waiting.start();
        MPI_Probe(0, MESSAGE_OFFSETS, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
        timers.waiting.stop();

        timers.communication.start();
        MPI_Recv(offsets, 2, MPI_INT, 0, MESSAGE_OFFSETS, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
        timers.communication.stop();

        while (offsets[0] != offsets[1]) {
            vector<string> lines;
            lines.reserve(chunkSize);

            int length = offsets[1] - offsets[0];
            char chunk[length];

            // Read the chunk from the input
            timers.loading.start();
            readChunk(chunk, input, offsets[0], offsets[1]);
            chunk[length - 1] = '\0';
            timers.loading.stop();

            // Process it
            timers.processing.start();

            char *next = strtok(chunk, "\n");

            while (next) {
                lines.emplace_back(next);
                next = strtok(nullptr, "\n");
            }

            results.process(columns, lines);
            timers.processing.stop();

            // Inform the master that the chunk has been processed
            timers.communication.start();
            MPI_Request request;
            MPI_Isend(nullptr, 0, MPI_INT, 0, MESSAGE_FINISHED, MPI_COMM_WORLD, &request);
            MPI_Wait(&request, MPI_STATUS_IGNORE);
            timers.communication.stop();

            // Wait for the next chunk
            timers.waiting.start();
            MPI_Probe(0, MESSAGE_OFFSETS, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
            timers.waiting.stop();

            // Receive the next chunk
            timers.communication.start();
            MPI_Recv(offsets, 2, MPI_INT, 0, MESSAGE_OFFSETS, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
            timers.communication.stop();
        }

        timers.communication.start();
        results.collect(0, MPI_COMM_WORLD);
        timers.communication.stop();

        timers.total.stop();

        // Send the time statistics
        unsigned long long localProcessingTime = timers.processing.elapsed();
        unsigned long long averageProcessingTime;
        MPI_Reduce(&localProcessingTime, &averageProcessingTime, 1, MPI_UNSIGNED_LONG_LONG, MPI_SUM, 0, MPI_COMM_WORLD);
    }

    return EXIT_SUCCESS;
}
