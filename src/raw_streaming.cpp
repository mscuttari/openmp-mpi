#include <cstring>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <mpi.h>
#include "reader.h"
#include "timer.h"
#include "worker.h"

#define CHUNK_SIZE_DEFAULT  300
#define BUFFER_SIZE         600

/**
 * The master process reads the local file.
 * Each process (master included) then receives a chunk of lines (raw text) to be processed.
 */

using namespace std;

struct Timers {
    Timers() = default;
    Timer total;
    Timer loading;
    Timer communication;
    Timer processing;
    Timer waiting;
    Timer output;
};

int raw_streaming(int argc, char *argv[]) {
    int rank, nProcs, chunkSize;
    Timers timers;

    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &nProcs);

    if (rank == 0 && argc < 3) {
        cerr << "Usage: " << argv[0] << " <data_file> <output_file> [chunk_size]" << endl;
        return EXIT_FAILURE;
    }

    if (argc == 4) {
        chunkSize = strtol(argv[3], nullptr, 10);
    } else {
        chunkSize = CHUNK_SIZE_DEFAULT;
    }

    timers.total.start();

    ifstream input;
    ofstream output;
    int size;

    if (rank == 0) {
        input.open(argv[1], ios::binary);

        if (!input.is_open()) {
            cerr << "Could not open the input file " << argv[1] << endl;
            return EXIT_FAILURE;
        }

        output.open(argv[2]);

        if (!output.is_open()) {
            cerr << "could not open the output file " << argv[2] << endl;
            return EXIT_FAILURE;
        }

        size = getFileSize(input);

        // Print information
        cout << "Chunk size: " << chunkSize << endl;
        cout << "File size: " << size << " bytes" << endl;

        cout << std::fixed << std::setprecision(2);
        cout << "Progress: 0.00%";
    }

    timers.communication.start();
    // All the processes must be informed about the file size, in order for
    // them to know when the whole file has been read.
    MPI_Bcast(&size, 1, MPI_INT, 0, MPI_COMM_WORLD);
    timers.communication.stop();

    // Skip the initial BOM bytes
    input.seekg(3);

    // Get the columns line
    string columnsLine;

    {
        if (rank == 0) {
            timers.loading.start();
            getline(input, columnsLine);
            timers.loading.stop();
        }

        // Send the line to the other processes
        int bufferSize = columnsLine.length();

        if (rank == 0 && columnsLine[bufferSize - 1] == '\r') {
            bufferSize--;
        }

        timers.communication.start();
        MPI_Bcast(&bufferSize, 1, MPI_INT, 0, MPI_COMM_WORLD);
        timers.communication.stop();

        columnsLine.resize(bufferSize);

        timers.communication.start();
        MPI_Bcast((char*) columnsLine.c_str(), bufferSize, MPI_CHAR, 0, MPI_COMM_WORLD);
        timers.communication.stop();
    }

    // Get the columns positions
    timers.processing.start();
    map<string, int> columns = mapColumns(columnsLine);
    timers.processing.stop();

    int position = input.tellg();
    char *allLines = new char[nProcs * chunkSize * BUFFER_SIZE];    // All the lines read by the master
    char *recvBuff = new char[chunkSize * BUFFER_SIZE];             // Lines to be processed by the current worker
    Results results;

    do {
        memset(allLines, 0, sizeof(char) * nProcs * chunkSize * BUFFER_SIZE);

        if (rank == 0) {

            for (int proc = 0; proc < nProcs; proc++) {
                int chunk = 0;

                while (chunk < chunkSize) {
                    timers.loading.start();
                    input.getline(&(allLines[(proc * chunkSize + chunk) * BUFFER_SIZE]), BUFFER_SIZE);
                    timers.loading.stop();

                    chunk++;
                }
            }

            position = input.tellg();
        }

        // Wait for all the processes to complete their previous chunk
        timers.waiting.start();
        MPI_Barrier(MPI_COMM_WORLD);
        timers.waiting.stop();

        vector<string> processLines;
        processLines.reserve(chunkSize);

        timers.communication.start();

        // The current position is sent to all nodes. This way each worker can understand,
        // by comparing it with the file size, if the whole file has been processed.
        MPI_Bcast(&position, 1, MPI_INT, 0, MPI_COMM_WORLD);

        // Send a chunk to each worker
        MPI_Scatter(allLines, chunkSize * BUFFER_SIZE, MPI_CHAR, recvBuff, chunkSize * BUFFER_SIZE, MPI_CHAR, 0, MPI_COMM_WORLD);

        timers.communication.stop();

        // Process it
        timers.processing.start();

        for (int i = 0; i < chunkSize; i++) {
            // Skip the line if its first character is the terminator
            if (recvBuff[i * BUFFER_SIZE] != '\0') {
                processLines.emplace_back(&(recvBuff[i * BUFFER_SIZE]));
            }
        }

        results.process(columns, processLines);
        timers.processing.stop();

        // Show progress
        if (rank == 0) {
            long double progress = position == -1 ? 100 : (long double) position / size * 100;
            cout << "\rProgress: " << progress << "%";

            if (position == -1 || position == size) {
                cout << "\n" << endl;
            }
        }

    } while (position < size && position != -1);

    // Wait for all the processes to finish
    timers.waiting.start();
    MPI_Barrier(MPI_COMM_WORLD);
    timers.waiting.stop();

    // Collect results
    timers.communication.start();
    results.collect(0, MPI_COMM_WORLD);
    timers.communication.stop();

    // Write results to file
    if (rank == 0) {
        timers.output.start();
        results.write(output);
        timers.output.stop();
    }

    timers.total.stop();

    // Get the average execution time
    unsigned long long localProcessingTime = timers.processing.elapsed();
    unsigned long long averageProcessingTime;

    MPI_Reduce(&localProcessingTime, &averageProcessingTime, 1, MPI_UNSIGNED_LONG_LONG, MPI_SUM, 0, MPI_COMM_WORLD);
    averageProcessingTime /= nProcs;

    // Print the execution time
    if (rank == 0) {
        cout << "Time statistics:\n"
             << " - Total: "            << timers.total.elapsed()         << " ms\n"
             << " - Loading: "          << timers.loading.elapsed()       << " ms\n"
             << " - Communication: "    << timers.communication.elapsed() << " ms\n"
             << " - Processing (avg): " << averageProcessingTime          << " ms\n"
             << " - Waiting: "          << timers.waiting.elapsed()       << " ms\n"
             << " - Output: "           << timers.output.elapsed()        << " ms\n"
             << endl;
    }

    // Free the resources
    delete[] recvBuff;
    delete[] allLines;

    return EXIT_SUCCESS;
}
