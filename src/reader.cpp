#include <ctime>
#include <iomanip>
#include <map>
#include <sstream>
#include <string>
#include <vector>
#include "reader.h"

using namespace std;

Accident::Accident(const map<string, int> &columns, const string &line) {
    vector<string> values;

    split(line, ',', [&values] (const string &value) {
        values.push_back(value);
    });

    istringstream(values[columns.at("DATE")]) >> get_time(&date, "%m/%d/%Y");
    istringstream(values[columns.at("TIME")]) >> get_time(&date, "%H:%M");
    mktime(&date);

    borough = values[columns.at("BOROUGH")];
    istringstream(values[columns.at("ZIP CODE")]) >> zipCode;
    istringstream(values[columns.at("LATITUDE")]) >> latitude;
    istringstream(values[columns.at("LONGITUDE")]) >> longitude;
    onStreetName = values[columns.at("ON STREET NAME")];
    crossStreetName = values[columns.at("CROSS STREET NAME")];
    offStreetName = values[columns.at("OFF STREET NAME")];
    istringstream(values[columns.at("NUMBER OF PERSONS INJURED")]) >> peopleInjured;
    istringstream(values[columns.at("NUMBER OF PERSONS KILLED")]) >> peopleKilled;
    istringstream(values[columns.at("NUMBER OF PEDESTRIANS INJURED")]) >> pedestriansInjured;
    istringstream(values[columns.at("NUMBER OF PEDESTRIANS KILLED")]) >> pedestriansKilled;
    istringstream(values[columns.at("NUMBER OF CYCLIST INJURED")]) >> cyclistsInjured;
    istringstream(values[columns.at("NUMBER OF CYCLIST KILLED")]) >> cyclistsKilled;
    istringstream(values[columns.at("NUMBER OF MOTORIST INJURED")]) >> motoristsInjured;
    istringstream(values[columns.at("NUMBER OF MOTORIST KILLED")]) >> motoristsKilled;
    contributingFactors[0] = values[columns.at("CONTRIBUTING FACTOR VEHICLE 1")];
    contributingFactors[1] = values[columns.at("CONTRIBUTING FACTOR VEHICLE 2")];
    contributingFactors[2] = values[columns.at("CONTRIBUTING FACTOR VEHICLE 3")];
    contributingFactors[3] = values[columns.at("CONTRIBUTING FACTOR VEHICLE 4")];
    contributingFactors[4] = values[columns.at("CONTRIBUTING FACTOR VEHICLE 5")];
    istringstream(values[columns.at("UNIQUE KEY")]) >> key;
    vehicleType[0] = values[columns.at("VEHICLE TYPE CODE 1")];
    vehicleType[1] = values[columns.at("VEHICLE TYPE CODE 2")];
    vehicleType[2] = values[columns.at("VEHICLE TYPE CODE 3")];
    vehicleType[3] = values[columns.at("VEHICLE TYPE CODE 4")];
    vehicleType[4] = values[columns.at("VEHICLE TYPE CODE 5")];
}

bool Accident::isLethal() {
    return peopleKilled != 0 || pedestriansKilled != 0 || cyclistsKilled != 0 || motoristsKilled != 0;
}

int getFileSize(istream &file) {
    int position = file.tellg();
    file.seekg(0, ios::end);
    int size = file.tellg();
    file.seekg(position, ios::beg);
    return size;
}

extern void readChunk(char *buffer, istream &file, int start, int end) {
    int position = file.tellg();
    file.clear();
    file.seekg(start, ios::beg);
    file.read(buffer, end - start);
    file.seekg(position, ios::beg);
}

template<typename StringFunction>
void split(const string &str, char delimiter, StringFunction f) {
    size_t from = 0;
    bool inString = false;

    for (size_t i = 0; i < str.length(); i++) {
        if (str[i] == '"') {
            inString = !inString;

        } else if (str[i] == delimiter && !inString) {
            f(str.substr(from, i - from));
            from = i + 1;
        }
    }

    if (from <= str.length()) {
        f(str.substr(from, str.length() - from));
    }
}

map<string, int> mapColumns(const string &columnsLine) {
    map<string, int> result;
    int pos = 0;

    split(columnsLine, ',', [&result, &pos] (const string &value) {
        result[value] = pos++;
    });

    return result;
}
