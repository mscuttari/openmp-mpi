#include <iostream>
#include <vector>
#include <mpi.h>
#include <omp.h>
#include "reader.h"
#include "worker.h"
#include "lib/fort.hpp"

#define MESSAGE_WEEKS_RANGE                         1000
#define MESSAGE_ACCIDENTS_PER_WEEK                  1001
#define MESSAGE_ACCIDENTS_PER_CONTRIBUTING_FACTOR   1002
#define MESSAGE_ACCIDENTS_PER_BOROUGH               1003
#define MESSAGE_DATA_FINISHED                       1004

using namespace std;

YearWeek::YearWeek(int year, int week) {
    year_ = year;
    week_ = week;
    initialized = true;
}

YearWeek::YearWeek(const tm &tm) {
    year_ = tm.tm_year + 1900;
    week_ = getWeekNumber(tm);
    initialized = true;
}

int YearWeek::year() const {
    return year_;
}

int YearWeek::week() const {
    return week_;
}

YearWeek& YearWeek::operator = (const YearWeek& o) {
    if (this != &o) {
        year_ = o.year_;
        week_ = o.week_;
        initialized = year_ != -1 || week_ != -1;
    }

    return *this;
}

bool YearWeek::operator == (const YearWeek &o) const {
    return year_ == o.year_ && week_ == o.week_;
}

bool YearWeek::operator < (const YearWeek &o) const {
    if (!initialized)
        return false;

    if (!o.initialized)
        return true;

    if (year_ == o.year_)
        return week_ < o.week_;

    return year_ < o.year_;
}

bool YearWeek::operator > (const YearWeek &o) const {
    if (!initialized)
        return false;

    if (!o.initialized)
        return true;

    if (year_ == o.year_)
        return week_ > o.week_;

    return year_ > o.year_;
}

int YearWeek::getWeekNumber(const tm &tm) {
    constexpr int DAYS_PER_WEEK = 7;
    const int wday = tm.tm_wday;
    const int delta = wday ? wday - 1 : DAYS_PER_WEEK - 1 ;
    return (tm.tm_yday + DAYS_PER_WEEK - delta) / DAYS_PER_WEEK ;
}

void Results::process(const map<string, int> &columns, const vector<string>& lines) {
    Results threadsResults[omp_get_max_threads()];

    // Map phase (parallel)
    #pragma omp parallel num_threads(omp_get_max_threads()) shared(threadsResults)
    {
        // Split the vector among the threads
        int thread = omp_get_thread_num();
        unsigned long long chunkSize = lines.size() / omp_get_num_threads();
        unsigned long long startPos = thread * chunkSize;
        unsigned long long endPos = thread == omp_get_num_threads() - 1 ? lines.size() : (thread + 1) * chunkSize;

        for (unsigned long long i = startPos; i < endPos; i++) {
            Accident accident(columns, lines[i]);
            YearWeek yearWeek(accident.date);

            if (yearWeek < start) {
                start = yearWeek;
            }

            if (yearWeek > end) {
                end = yearWeek;
            }

            if (accident.isLethal()) {
                threadsResults[thread].lethalAccidentsPerWeek[yearWeek]++;
            }

            for (const auto & contributingFactor : accident.contributingFactors) {
                if (contributingFactor.length() == 0)
                    continue;

                threadsResults[thread].accidentsPerContributingFactor[contributingFactor]++;

                if (accident.isLethal()) {
                    threadsResults[thread].lethalAccidentsPerContributingFactor[contributingFactor]++;
                }
            }

            if (accident.borough.length() != 0) {
                threadsResults[thread].accidentsPerBorough[accident.borough]++;
                threadsResults[thread].accidentsPerBoroughAndWeek[accident.borough][yearWeek]++;

                if (accident.isLethal()) {
                    threadsResults[thread].lethalAccidentsPerBorough[accident.borough]++;
                }
            }
        }
    }

    // Reduce phase (sequential)
    for (auto const& threadResults : threadsResults) {
        for (auto const& entry : threadResults.lethalAccidentsPerWeek) {
            lethalAccidentsPerWeek[entry.first] += entry.second;
        }

        for (auto const& entry : threadResults.accidentsPerContributingFactor) {
            accidentsPerContributingFactor[entry.first] += entry.second;
        }

        for (auto const& entry : threadResults.lethalAccidentsPerContributingFactor) {
            lethalAccidentsPerContributingFactor[entry.first] += entry.second;
        }

        for (auto const& entry : threadResults.accidentsPerBorough) {
            accidentsPerBorough[entry.first] += entry.second;
        }

        for (auto const& boroughEntry : threadResults.accidentsPerBoroughAndWeek) {
            for (auto const& weekEntry : boroughEntry.second) {
                accidentsPerBoroughAndWeek[boroughEntry.first][weekEntry.first] += weekEntry.second;
            }
        }

        for (auto const& entry : threadResults.lethalAccidentsPerBorough) {
            lethalAccidentsPerBorough[entry.first] += entry.second;
        }
    }
}

void Results::write(ostream &output) {
    {
        fort::char_table table;
        table << fort::header << "YEAR" << "WEEK" << "LETHAL ACCIDENTS" << fort::endr;

        for (auto const& entry : lethalAccidentsPerWeek) {
            table << entry.first.year() << entry.first.week() + 1 << entry.second << fort::endr;
        }

        output << table.to_string() << endl;
    }

    {
        fort::char_table table;
        table << fort::header << "CONTRIBUTING FACTOR" << "ACCIDENTS" << "LETHALITY (%)" << fort::endr;

        for (auto const& entry : accidentsPerContributingFactor) {
            float lethalityPercentage = (float) lethalAccidentsPerContributingFactor[entry.first] * 100 / entry.second;
            table << entry.first << entry.second << lethalityPercentage << fort::endr;
        }

        output << table.to_string() << endl;
    }

    {
        fort::char_table table;
        table << fort::header << "BOROUGH" << "ACCIDENTS" << "AVERAGE WEEK LETHALITY (%)" << fort::endr;

        int numberOfWeeks = (end.year() - start.year()) * 52 + end.week() - end.week() + 1;

        for (auto const& boroughEntry : accidentsPerBorough) {
            float avgLethalsPerWeek = lethalAccidentsPerBorough[boroughEntry.first] / (float) numberOfWeeks;
            table << boroughEntry.first << boroughEntry.second << avgLethalsPerWeek << fort::endr;
        }

        output << table.to_string() << endl;
    }

    {
        fort::char_table table;
        table << fort::header << "BOROUGH" << "YEAR" << "WEEK" << "ACCIDENTS" << fort::endr;

        for (auto const& boroughEntry : accidentsPerBorough) {
            for (auto const& weekEntry : accidentsPerBoroughAndWeek[boroughEntry.first]) {
                table << boroughEntry.first << weekEntry.first.year() << weekEntry.first.week() + 1 << weekEntry.second << fort::endr;
            }
        }

        output << table.to_string() << endl;
    }
}

void Results::collect(int sink, MPI_Comm comm) {
    int rank, nProcs;

    MPI_Comm_rank(comm, &rank);
    MPI_Comm_size(comm, &nProcs);

    if (rank == sink) {
        for (int i = 0; i < nProcs - 1; i++) {
            MPI_Status status;

            do {
                MPI_Probe(MPI_ANY_SOURCE, MPI_ANY_TAG, comm, &status);

                if (status.MPI_TAG == MESSAGE_WEEKS_RANGE) {
                    // Receive start and end weeks
                    int values[4];
                    MPI_Recv(values, 4, MPI_INT, status.MPI_SOURCE, status.MPI_TAG, comm, MPI_STATUS_IGNORE);

                    YearWeek recvStart(values[0], values[1]);
                    YearWeek recvEnd(values[2], values[3]);

                    if (recvStart < start) {
                        start = recvStart;
                    }

                    if (recvEnd > end) {
                        end = recvEnd;
                    }

                } else if (status.MPI_TAG == MESSAGE_ACCIDENTS_PER_WEEK) {
                    // Receive per week statistics
                    int values[3];
                    MPI_Recv(values, 3, MPI_INT, status.MPI_SOURCE, status.MPI_TAG, comm, MPI_STATUS_IGNORE);
                    YearWeek yearWeek{values[0], values[1]};
                    lethalAccidentsPerWeek[yearWeek] += values[2];

                } else if (status.MPI_TAG == MESSAGE_ACCIDENTS_PER_CONTRIBUTING_FACTOR) {
                    // Receive per contributing factor statistics
                    int length;
                    MPI_Get_count(&status, MPI_CHAR, &length);

                    string contributingFactor;
                    contributingFactor.resize(length);
                    MPI_Recv((char*) contributingFactor.c_str(), length, MPI_CHAR, status.MPI_SOURCE, status.MPI_TAG, comm, MPI_STATUS_IGNORE);

                    int values[2];
                    MPI_Recv(values, 2, MPI_INT, status.MPI_SOURCE, status.MPI_TAG, comm, MPI_STATUS_IGNORE);

                    accidentsPerContributingFactor[contributingFactor] += values[0];
                    lethalAccidentsPerContributingFactor[contributingFactor] += values[1];

                } else if (status.MPI_TAG == MESSAGE_ACCIDENTS_PER_BOROUGH) {
                    // Receive per borough statistics
                    int length;
                    MPI_Get_count(&status, MPI_CHAR, &length);

                    string borough;
                    borough.resize(length);
                    MPI_Recv((char*) borough.c_str(), length, MPI_CHAR, status.MPI_SOURCE, status.MPI_TAG, comm, MPI_STATUS_IGNORE);

                    int values[3];
                    MPI_Recv(values, 3, MPI_INT, status.MPI_SOURCE, status.MPI_TAG, comm, MPI_STATUS_IGNORE);

                    accidentsPerBorough[borough] += values[0];
                    lethalAccidentsPerBorough[borough] += values[1];

                    while (values[2]-- > 0) {
                        int weekValues[3];
                        MPI_Recv(weekValues, 3, MPI_INT, status.MPI_SOURCE, status.MPI_TAG, comm, MPI_STATUS_IGNORE);
                        YearWeek week(weekValues[0], weekValues[1]);
                        accidentsPerBoroughAndWeek[borough][week] += weekValues[2];
                    }
                }

            } while (status.MPI_TAG != MESSAGE_DATA_FINISHED);

            MPI_Recv(nullptr, 0, MPI_CHAR, MPI_ANY_SOURCE, MESSAGE_DATA_FINISHED, comm, MPI_STATUS_IGNORE);
        }

    } else {
        {
            // Send start and end weeks
            MPI_Request request;
            int values[4] = {start.year(), start.week(), end.year(), end.week()};
            MPI_Isend(values, 4, MPI_INT, sink, MESSAGE_WEEKS_RANGE, comm, &request);
            MPI_Wait(&request, MPI_STATUS_IGNORE);
        }

        {
            // Send per week statistics
            int i = 0;
            int amount = lethalAccidentsPerWeek.size();
            MPI_Request requests[amount];

            for (auto const& entry : lethalAccidentsPerWeek) {
                int yearWeek[3] = {entry.first.year(), entry.first.week(), entry.second};
                MPI_Isend(yearWeek, 3, MPI_INT, sink, MESSAGE_ACCIDENTS_PER_WEEK, comm, &(requests[i++]));
            }

            MPI_Waitall(i, requests, MPI_STATUS_IGNORE);
        }

        {
            // Send per contributing factor statistics
            for (auto const& entry : accidentsPerContributingFactor) {
                MPI_Request requests[2];
                MPI_Isend(entry.first.c_str(), entry.first.length(), MPI_CHAR, sink, MESSAGE_ACCIDENTS_PER_CONTRIBUTING_FACTOR, comm, &(requests[0]));
                int values[2] = {accidentsPerContributingFactor[entry.first], lethalAccidentsPerContributingFactor[entry.first]};
                MPI_Isend(values, 2, MPI_INT, sink, MESSAGE_ACCIDENTS_PER_CONTRIBUTING_FACTOR, comm, &(requests[1]));
                MPI_Waitall(2, requests, MPI_STATUS_IGNORE);
            }
        }

        {
            // Send per borough statistics
            for (auto const& boroughEntry : accidentsPerBorough) {
                {
                    MPI_Request requests[2];
                    int values[3] = {accidentsPerBorough[boroughEntry.first],
                                     lethalAccidentsPerBorough[boroughEntry.first],
                                     (int) accidentsPerBoroughAndWeek[boroughEntry.first].size()};

                    MPI_Isend(boroughEntry.first.c_str(), boroughEntry.first.length(), MPI_CHAR, sink, MESSAGE_ACCIDENTS_PER_BOROUGH, comm, &(requests[0]));
                    MPI_Isend(values, 3, MPI_INT, sink, MESSAGE_ACCIDENTS_PER_BOROUGH, comm, &(requests[1]));
                    MPI_Waitall(2, requests, MPI_STATUS_IGNORE);
                }

                for (auto const& weekEntry : accidentsPerBoroughAndWeek[boroughEntry.first]) {
                    int values[3] = {weekEntry.first.year(), weekEntry.first.week(), weekEntry.second};
                    MPI_Request request;
                    MPI_Isend(values, 3, MPI_INT, sink, MESSAGE_ACCIDENTS_PER_BOROUGH, comm, &request);
                    MPI_Wait(&request, MPI_STATUS_IGNORE);
                }
            }
        }

        {
            // Send termination message
            MPI_Request request;
            MPI_Isend(nullptr, 0, MPI_CHAR, 0, MESSAGE_DATA_FINISHED, MPI_COMM_WORLD, &request);
            MPI_Wait(&request, MPI_STATUS_IGNORE);
        }
    }
}
