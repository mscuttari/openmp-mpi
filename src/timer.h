#ifndef OPENMP_MPI_TIMER_H
#define OPENMP_MPI_TIMER_H

#include <chrono>

class Timer {
public:
    Timer();

    /** Start the timer. */
    void start();

    /** Stop the timer and get the elapsed time in milliseconds. */
    void stop();

    /** Get the total elapsed time in milliseconds. */
    unsigned long long elapsed();

    /** Reset the elapsed time. */
    void reset();

private:
    bool running = false;
    std::chrono::high_resolution_clock::time_point time;
    unsigned long long total = 0;
};

#endif // OPENMP_MPI_TIMER_H
