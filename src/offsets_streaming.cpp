#include <cstring>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <mpi.h>
#include "reader.h"
#include "timer.h"
#include "worker.h"

#define CHUNK_SIZE_DEFAULT 300

/**
 * The master process determines the offsets of the chunks.
 * The offsets (two integers: start and end of chunk) are sent to each worker,
 * which will individually read it from the file and process it.
 */

using namespace std;

struct Timers {
    Timers() = default;
    Timer total;
    Timer loading;
    Timer communication;
    Timer processing;
    Timer waiting;
    Timer output;
};

int offsets_streaming(int argc, char *argv[]) {
    int rank, nProcs, chunkSize;
    Timers timers;

    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &nProcs);

    if (rank == 0 && argc < 3) {
        cerr << "Usage: " << argv[0] << " <data_file> <output_file> [chunk_size]" << endl;
        return EXIT_FAILURE;
    }

    if (argc == 4) {
        chunkSize = strtol(argv[3], nullptr, 10);
    } else {
        chunkSize = CHUNK_SIZE_DEFAULT;
    }

    timers.total.start();

    // Open the input file on all the processes
    ifstream input(argv[1], ios::binary);

    if (!input.is_open()) {
        cerr << "Could not open the input file " << argv[1] << endl;
        return EXIT_FAILURE;
    }

    int size = getFileSize(input);

    // Open the output file only on the master process
    ofstream output;

    if (rank == 0) {
        output.open(argv[2]);

        if (!output.is_open()) {
            cerr << "Could not open the output file " << argv[2] << endl;
            return EXIT_FAILURE;
        }
    }

    // Print information
    if (rank == 0) {
        cout << "Chunk size: " << chunkSize << endl;
        cout << "File size: " << size << " bytes" << endl;

        cout << std::fixed << std::setprecision(2);
        cout << "Progress: 0.00%";
    }

    // Skip the initial BOM bytes
    input.seekg(3);

    // Get the columns line
    string columnsLine;

    if (rank == 0) {
        timers.loading.start();
        getline(input, columnsLine);
        timers.loading.stop();
    }

    {
        // Send the line to the other processes
        unsigned long long bufferSize = columnsLine.length();

        if (rank == 0 && columnsLine[bufferSize - 1] == '\r') {
            bufferSize--;
        }

        timers.communication.start();
        MPI_Bcast(&bufferSize, 1, MPI_UNSIGNED_LONG_LONG, 0, MPI_COMM_WORLD);
        timers.communication.stop();

        columnsLine.resize(bufferSize);

        timers.communication.start();
        MPI_Bcast((char*) columnsLine.c_str(), (int) bufferSize, MPI_CHAR, 0, MPI_COMM_WORLD);
        timers.communication.stop();
    }

    // Get the columns positions
    timers.processing.start();
    map<string, int> columns = mapColumns(columnsLine);
    timers.processing.stop();

    int position = input.tellg();
    int *chunks = new int[nProcs * 2];
    Results results;

    do {
        input.seekg(position, ios::beg);
        memset(chunks, 0, sizeof(int) * nProcs * 2);

        if (rank == 0) {
            timers.loading.start();

            for (int proc = 0; proc < nProcs; proc++) {
                if (position == size || position == -1) {
                    // Finished reading the input
                    break;
                }

                chunks[proc * 2] = position;

                int lines = 0;
                for (string line; lines < chunkSize && getline(input, line); lines++);

                position = input.tellg();

                if (position == -1) {
                    chunks[proc * 2 + 1] = size;
                } else {
                    chunks[proc * 2 + 1] = position;
                }
            }

            timers.loading.stop();
        }

        // Wait for all the processes to complete their previous chunk
        timers.waiting.start();
        MPI_Barrier(MPI_COMM_WORLD);
        timers.waiting.stop();

        int offsets[2];
        vector<string> lines;
        lines.reserve(chunkSize);

        // Receive the offsets
        timers.communication.start();
        MPI_Bcast(&position, 1, MPI_INT, 0, MPI_COMM_WORLD);
        MPI_Scatter(chunks, 2, MPI_INT, offsets, 2, MPI_INT, 0, MPI_COMM_WORLD);
        timers.communication.stop();

        if (offsets[0] != offsets[1]) {
            int length = offsets[1] - offsets[0];
            char *chunk = new char[length];

            // Read the chunk from the input
            timers.loading.start();
            readChunk(chunk, input, offsets[0], offsets[1]);
            chunk[length - 1] = '\0';
            timers.loading.stop();

            // Process it
            timers.processing.start();

            char *next = strtok(chunk, "\n");

            while (next) {
                lines.emplace_back(next);
                next = strtok(nullptr, "\n");
            }

            results.process(columns, lines);
            timers.processing.stop();

            delete[] chunk;

            // Show progress
            if (rank == 0) {
                long double progress = position == -1 ? 100 : (long double) position / size * 100;
                cout << "\rProgress: " << progress << "%";

                if (position == -1 || position == size) {
                    cout << "\n" << endl;
                }
            }
        }

    } while (position < size && position != -1);

    // Wait for all the processes to finish
    timers.waiting.start();
    MPI_Barrier(MPI_COMM_WORLD);
    timers.waiting.stop();

    // Collect results
    timers.communication.start();
    results.collect(0, MPI_COMM_WORLD);
    timers.communication.stop();

    // Write results to file
    if (rank == 0) {
        timers.output.start();
        results.write(output);
        timers.output.stop();
    }

    timers.total.stop();

    // Get average execution time
    unsigned long long localProcessingTime = timers.processing.elapsed();
    unsigned long long averageProcessingTime;

    MPI_Reduce(&localProcessingTime, &averageProcessingTime, 1, MPI_UNSIGNED_LONG_LONG, MPI_SUM, 0, MPI_COMM_WORLD);
    averageProcessingTime /= nProcs;

    if (rank == 0) {
        cout << "Time statistics:\n"
             << " - Total: "            << timers.total.elapsed()         << " ms\n"
             << " - Loading: "          << timers.loading.elapsed()       << " ms\n"
             << " - Communication: "    << timers.communication.elapsed() << " ms\n"
             << " - Processing (avg): " << averageProcessingTime          << " ms\n"
             << " - Waiting: "          << timers.waiting.elapsed()       << " ms\n"
             << " - Output: "           << timers.output.elapsed()        << " ms\n"
             << endl;
    }

    // Free the resources
    delete[] chunks;

    return EXIT_SUCCESS;
}
