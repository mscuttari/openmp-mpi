#ifndef OPENMP_MPI_WORKER_H
#define OPENMP_MPI_WORKER_H

#include <map>
#include <vector>
#include <mpi.h>

class YearWeek {
public:
    YearWeek() = default;
    YearWeek(int year, int week);
    explicit YearWeek(const tm &tm);

    int year() const;
    int week() const;

    YearWeek& operator = (const YearWeek& o);
    bool operator == (const YearWeek &o) const;
    bool operator < (const YearWeek &o) const;
    bool operator > (const YearWeek &o) const;

private:
    bool initialized = false;
    int year_ = -1;
    int week_ = -1;

    /** Get the week number (within a year) of a date */
    static int getWeekNumber(const tm &tm);
};

class Results {
public:
    Results() = default;

    YearWeek start;
    YearWeek end;

    std::map<YearWeek, int> lethalAccidentsPerWeek;

    std::map<std::string, int> accidentsPerContributingFactor;
    std::map<std::string, int> lethalAccidentsPerContributingFactor;

    std::map<std::string, int> accidentsPerBorough;
    std::map<std::string, int> lethalAccidentsPerBorough;
    std::map<std::string, std::map<YearWeek, int>> accidentsPerBoroughAndWeek;

    /** Process a chunk of lines and extracts the useful information from it. */
    void process(const std::map<std::string, int> &columns, const std::vector<std::string>& lines);

    /** Collect all the data into one sink process. */
    void collect(int sink, MPI_Comm comm);

    /** Write the results to a file. */
    void write(std::ostream &output);
};

/** Create a map between each column name and its position. */
extern std::map<std::string, int> mapColumns(const std::string &columnsLine);

#endif // OPENMP_MPI_WORKER_H
