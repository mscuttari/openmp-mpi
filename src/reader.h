#ifndef OPENMP_MPI_READER_H
#define OPENMP_MPI_READER_H

#include <map>
#include <string>

class Accident {
public:
    Accident(const std::map<std::string, int> &columns, const std::string &line);
    struct tm date {};
    std::string borough;
    int zipCode = 0;
    double latitude = 0;
    double longitude = 0;
    std::string onStreetName;
    std::string crossStreetName;
    std::string offStreetName;
    int peopleInjured = 0;
    int peopleKilled = 0;
    int pedestriansInjured = 0;
    int pedestriansKilled = 0;
    int cyclistsInjured = 0;
    int cyclistsKilled = 0;
    int motoristsInjured = 0;
    int motoristsKilled = 0;
    std::string contributingFactors[5];
    int key = 0;
    std::string vehicleType[5];

    /** Get whether the accident killed someone. */
    bool isLethal();
};

/** Get the size of a file (in bytes). The file should be opened in binary mode. */
extern int getFileSize(std::istream &file);

/** Read a chunk of data from the file. */
extern void readChunk(char *buffer, std::istream &file, int start, int end);

/** Split a string given a delimiter. Each substring offset is passed to the lambda function f. */
template<typename StringFunction>
extern void split(const std::string &str, char delimiter, StringFunction f);

#endif // OPENMP_MPI_READER_H
