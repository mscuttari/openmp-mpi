#include <iostream>
#include <mpi.h>

#define MODE_RAW_STREAMING      1
#define MODE_OFFSETS_STREAMING  2
#define MODE_OFFSETS_QUEUE      3

using namespace std;

extern int raw_streaming(int argc, char *argv[]);
extern int offsets_streaming(int argc, char *argv[]);
extern int offsets_queue(int argc, char *argv[]);

int main(int argc, char *argv[]) {
    if (argc < 2) {
        cerr << "Usage: " << argv[0] << " <operation_mode> [mode_params ...]" << endl;
        return EXIT_FAILURE;
    }

    int mode = strtol(argv[1], nullptr, 10);
    int rank;

    MPI_Init(&argc, &argv);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);

    argv[1] = argv[0];
    argc--;
    argv++;

    int result;

    if (mode == MODE_RAW_STREAMING) {
        result = raw_streaming(argc, argv);

    } else if (mode == MODE_OFFSETS_STREAMING) {
        result = offsets_streaming(argc, argv);

    } else if (mode == MODE_OFFSETS_QUEUE) {
        result = offsets_queue(argc, argv);

    } else {
        if (rank == 0) {
            cerr << "Invalid operation mode: " << mode << "\n"
                 << "Valid values:\n"
                 << "  " << MODE_RAW_STREAMING     << " - raw streaming\n"
                 << "  " << MODE_OFFSETS_STREAMING << " - offsets streaming\n"
                 << "  " << MODE_OFFSETS_QUEUE     << " - offsets streaming with queue\n"
                 << endl;
        }

        result = EXIT_FAILURE;
    }

    MPI_Finalize();
    return result;
}
