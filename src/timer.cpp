#include "timer.h"

using namespace std::chrono;

Timer::Timer() = default;

void Timer::start() {
    time = high_resolution_clock::now();
    running = true;
}

void Timer::stop() {
    if (running) {
        auto now = high_resolution_clock::now();
        running = false;
        total += std::chrono::duration_cast<std::chrono::nanoseconds>(now - time).count();
    }
}

unsigned long long Timer::elapsed() {
    if (running) {
        auto now = high_resolution_clock::now();
        return (total + std::chrono::duration_cast<std::chrono::nanoseconds>(now - time).count()) / 1000000;

    } else {
        return total / 1000000;
    }
}

void Timer::reset() {
    total = 0;
}